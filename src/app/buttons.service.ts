//simulating the consuption of an api / http call . demonstraded by the uneeded data below
// data used to build the operation buttons.
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ButtonsService {

  getButtons() {
    return [
      {
        sign: "+",
        action: (a: number, b: number) => a + b 
      },
      {
        sign: "-",
        action: (a: number, b: number) => a - b
      },
      {
        sign: "*",
        action: (a: number, b: number) => a * b 
      },
      {
        sign: "/",
        action: (a: number, b: number) =>  b > 0 ? a / b : undefined
      },
    ]
  }
  constructor() { }
}
