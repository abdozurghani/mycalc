import { Component, OnInit } from '@angular/core';
import { ButtonsService } from '../buttons.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  lcd="";
  reseted = true;
  operationButtons = [];
  numberButtons = [
     "1", "2", "3", "4", "5", "6", "7", "8", "9","0"
  ]
  
  operationClick(a){
    this.lcd+=a;
  }
  numberClick(a:string){
    this.reseted==true? this.lcd = a : this.lcd+=a;
    this.reseted = false;

  }
  result(){
    this.lcd=eval(this.lcd);
    
  }
  reset(){
    this.reseted = true;
    this.lcd = "";
  }
  //dependancy injection
  constructor(service :ButtonsService) {
    this.operationButtons = service.getButtons(); 
  }

  ngOnInit(): void {
    
  }

}
